# Sine Generator in Swift #

A Sine Generator built using Swift for a course on sound analysis. Includes a square wave generator, Butterworth filter and Biquad filter.