//
//  SineGenerator.swift
//  Sine Generator
//
//  Some code borrowed from Apple's 2015 WWDC
//  https://devstreaming-cdn.apple.com/videos/wwdc/2015/507pq8rldk/507/507_whats_new_in_core_audio.pdf
//  Created by Varshan S on 2021-02-05.
//


import Foundation
import AudioUnit
import AVFoundation
import Accelerate


enum Wave: String, CaseIterable, Identifiable {
    case sine
    case square
    var id: String { self.rawValue }
}


final class SineGenerator: NSObject {
    private var audioUnit: AUAudioUnit!
    
    enum Status {
        case initialize
        case playing
        case paused
    }
    
    private var status: Status = Status.initialize
    
    private var sampleRate: Double = 48000.0
    private var sampleRateFloat: Float = 48000.0
    private var frameCount: UInt32 = 0
    
    private var freq: Float = 440.0
    private var volume: Float = 16383.0
    private var phase: Float = 0.0
    
    /* The graphing functions **/
    private var fft: FFT = FFT()
    private var fftSize: Int = 4096
    private var signal: [Float] = []
    private var magnitudes: [Float] = []
    private var readyForFFTGraph: Bool = false
    
    /* Assignment 2 */
    private var readyForBiquad: Bool = false
    private var waveType: Wave = Wave.sine
    private var lowPass = Biquad.LowPass()
    private var resonance: Float = 1.0
    private var cutoff: Float = 1000.0
    private var buffer: [Float] = []
    
    /* Assignment 3 */
    private var readyForHann: Bool = false
    private var windowStarted: Bool = false
    private var cutoffButterworth: Float = 1000.0
    private var dcGain: Float = 1.0
    private var window = Window.Hann()
    
    /* multiple buffers to consume **/
    private var reversedSignalPreprocess1: [Int16] = []
    private var reversedSignalPreprocess2: [Int16] = []
    private var reversedSignalPreprocess3: [Int16] = []
    private var reversedSignalPreprocess4: [Int16] = []
    
    /* read index allows us to have variable frame count **/
    private var reversedSignalReadIndex = 0
    /* which of the 4 buffers to read from **/
    private var reversedSignalToReadFrom = 0
    
    /* to concurrently fill buffers **/
    private let concurrentQueue = DispatchQueue(label: "sine", attributes: .concurrent)
    private var semaphore: [DispatchSemaphore] = [DispatchSemaphore(value: 0),
                                                  DispatchSemaphore(value: 0),
                                                  DispatchSemaphore(value: 0),
                                                  DispatchSemaphore(value: 0)]
    /* to mutually exclude buffer fillers **/
    private let mutex = DispatchSemaphore(value: 1)
    
    /* GUI controls */
    func setResonance(resonance: Float) {
        self.resonance = Float(resonance)
        lowPass.computeCoeffs(resonance: self.resonance, cutoff: cutoff, sampleRate: sampleRateFloat)
    }
    
    func setWave(type: Wave) {
        waveType = type
    }
    
    func setCutoff(cutoff: Float) {
        self.cutoff = Float(cutoff)
        lowPass.computeCoeffs(resonance: resonance, cutoff: self.cutoff, sampleRate: sampleRateFloat)
    }
    
    func setButterworthCutoff(cutoff: Float) {
        self.cutoffButterworth = Float(cutoff)
        window.calculateButterworthCoeff(w0: Float(cutoffButterworth), DCGain: Float(dcGain))
    }
    
    func setButterworthDCGain(gain: Float) {
        self.dcGain = Float(gain)
        window.calculateButterworthCoeff(w0: Float(cutoffButterworth), DCGain: Float(dcGain))
    }
    
    func startWindow() {
        if status == Status.playing {
            window = Window.Hann()
            window.makeWindow(size: 4096, frameSize: 512)
            window.calculateButterworthCoeff(w0: Float(cutoffButterworth), DCGain: Float(dcGain))
            window.calculateHannCoeff()
            reversedSignalPreprocess1 = [Int16](repeating: 0, count: 2048)
            reversedSignalPreprocess2 = [Int16](repeating: 0, count: 2048)
            reversedSignalPreprocess3 = [Int16](repeating: 0, count: 2048)
            reversedSignalPreprocess4 = [Int16](repeating: 0, count: 2048)
        }
    }
    
    func setFrequency(newFreq: Float) {
        freq = newFreq
    }

    /* due to int16 limitations, we will keep the volume low **/
    func setToneVolume(vol: Float) {
        volume = vol * 16383.0/2
    }
    
    func performFFT() {
        if readyForFFTGraph {
            fft.forwardAndMagnitudes(signal: signal, magnitudes: &magnitudes)
        }
    }
    
    func toggleBiquad(ready: Bool) {
        readyForBiquad = ready
        if readyForBiquad {
            lowPass.computeCoeffs(resonance: resonance, cutoff: cutoff, sampleRate: sampleRateFloat)
        }
    }
    
    func toggleHann(ready: Bool) {
        if (!readyForHann && !windowStarted) {
            windowStarted = true
            /* calculate coefficients */
            startWindow()
            
            concurrentQueue.async {
                self.preprocess(id: 0)
            }
            
            concurrentQueue.async {
                self.preprocess(id: 1)
            }
            
            concurrentQueue.async {
                self.preprocess(id: 2)
            }
            
            concurrentQueue.async {
                self.preprocess(id: 3)
            }
        }
        readyForHann = ready
    }
    
    func fftStatus(ready: Bool) {
        readyForFFTGraph = ready
    }
    
    func performedFFT() -> Bool {
        return readyForFFTGraph
    }
    
    func getFreqData(freq: Float) -> Float {
        let index = Int(floorf(freq/((sampleRateFloat/2)/Float(fftSize/2))))
        return magnitudes[index]
    }
    
    /* we will process in chunks of 512, concurrently, regardless of framesize */
    func preprocess(id: Int) {
        var buffer = [Float](repeating: 0, count: 512)
        
        while true {
            semaphore[id].wait()
            mutex.wait()
            /* we increase the phase each frame */
            let phaseIncrement = 2.0 * Float.pi * self.freq / sampleRateFloat
            
            for _ in 0..<4 {
                for i in 0..<Int(buffer.count) {
                    var sinValue = sinf(self.phase)
                    
                    /* generate a "square" wave using harmonics */
                    if (waveType == Wave.square) {
                        sinValue += sinf(3*self.phase)/3
                        sinValue += sinf(5*self.phase)/5
                        sinValue += sinf(7*self.phase)/7
                        sinValue += sinf(9*self.phase)/9
                        sinValue += sinf(11*self.phase)/11
                        sinValue += sinf(13*self.phase)/13
                        sinValue += sinf(15*self.phase)/15
                        sinValue += sinf(17*self.phase)/17
                        sinValue += sinf(19*self.phase)/19
                    }
                    
                    /* compute the new phase using phaseIncrement */
                    self.phase += phaseIncrement
                                            
                    /* to prevent overflow */
                    if (self.phase > 2.0 * Float.pi) {
                        self.phase -= 2.0 * Float.pi
                    }
                    
                    buffer[i] = Float(sinValue)
                    
                    /* if biquad is being used, compute low pass filter */
                    if readyForBiquad {
                        buffer[i] = lowPass.processSample(input: buffer[i])
                    }
                    
                    /* multiply by volume */
                    buffer[i] = buffer[i] * Float(self.volume)
                }
            
                /* process frames **/
                self.window.shift(data: &buffer)
                
            }
            
            self.window.processData(id: id)
            
            for i in (0..<Int(2048)) {
                switch id {
                    case 0:
                        self.reversedSignalPreprocess1[2048-1-i] = Int16(sharedBuffer.shared.outBuffer1[i+2048])
                    case 1:
                        self.reversedSignalPreprocess2[2048-1-i] = Int16(sharedBuffer.shared.outBuffer2[i+2048])
                    case 2:
                        self.reversedSignalPreprocess3[2048-1-i] = Int16(sharedBuffer.shared.outBuffer3[i+2048])
                    case 3:
                        self.reversedSignalPreprocess4[2048-1-i] = Int16(sharedBuffer.shared.outBuffer4[i+2048])
                    default:
                        continue
                        //self.reversedSignalPreprocess1[2048-1-i] = Int16(sharedBuffer.shared.outBuffer1[i+2048])
                }
            }
            
            mutex.signal()
        }
    }

    /* generates our sine wave */
    func generate() {
        if status == Status.playing {
            audioUnit.stopHardware()
            fftStatus(ready: false)
            status = Status.paused
            return
        }
        
        /* start the audio buffer in a queue **/
        do {
            /* use standard output */
            let description = AudioComponentDescription(
                componentType: kAudioUnitType_Output,
                componentSubType: kAudioUnitSubType_DefaultOutput,
                componentManufacturer: kAudioUnitManufacturer_Apple,
                componentFlags: 0,
                componentFlagsMask: 0 )

            if (self.status == Status.initialize) {
                /* Make the audio unit */
                try self.audioUnit = AUAudioUnit(componentDescription: description)
                let inputBus = self.audioUnit.inputBusses[0]
                let audioFormat = AVAudioFormat(
                    commonFormat: AVAudioCommonFormat.pcmFormatInt16,
                    sampleRate: self.sampleRate,
                    channels:AVAudioChannelCount(1),
                    interleaved: false )
                try inputBus.setFormat(audioFormat ?? AVAudioFormat())
                
                self.audioUnit.outputProvider = {(
                    actionFlags,
                    timestamp,
                    frameCount,
                    inputBusNumber,
                    inputDataList) -> AUAudioUnitStatus in
                    
                    self.frameCount = frameCount
                    
                    /* To enable graphing, we can uncomment these lines **/
                    if (self.status == Status.initialize || self.status == Status.paused) {
                        //self.fftSize = Int(frameCount * 8)
                        
                        /* initialize all fft and signal arrays */
                        //self.signal = [Float](repeating: 0, count: self.fftSize)
                        //self.magnitudes = [Float](repeating: 0, count: self.fftSize/2)
                        //self.fft.start(size: self.fftSize)
                        //self.fftStatus(ready: true)
                        self.status = Status.playing
                    }
                    
                    self.render(inputDataList: inputDataList)
                    self.buffer = [Float](repeating: 0, count: Int(frameCount))
                    
                    return(0)
                }
                
            }

            self.audioUnit.isOutputEnabled = true
            try self.audioUnit.allocateRenderResources()
            try self.audioUnit.startHardware()
        } catch {/* error */}
    
    }

    /* call back for rendering **/
    private func render(
            inputDataList: UnsafeMutablePointer<AudioBufferList>) {
        let dataBuffer = UnsafeMutableAudioBufferListPointer(inputDataList)
        let bufferSize = dataBuffer.count
        
        if (bufferSize > 0) {
            let audioBuffer: AudioBuffer = dataBuffer[0]
            let count = Int(frameCount)
            let byteSizeCount = Int(audioBuffer.mDataByteSize)
            
            /* C pointer to audio buffer */
            let bufferPointer = UnsafeMutableRawPointer(audioBuffer.mData)
            if var pointer = bufferPointer {
                if (self.volume > 0) {
                    if readyForHann {
                        for i in 0..<count {
                            /* sanity check, to make sure C reference is valid */
                            if (i < (byteSizeCount / 2)) {
                                /* we will read from the buffers in order **/
                                switch reversedSignalToReadFrom {
                                    case 0:
                                        pointer.assumingMemoryBound(to: Int16.self).pointee = reversedSignalPreprocess1[reversedSignalReadIndex]
                                    case 1:
                                        pointer.assumingMemoryBound(to: Int16.self).pointee = reversedSignalPreprocess2[reversedSignalReadIndex]
                                    case 2:
                                        pointer.assumingMemoryBound(to: Int16.self).pointee = reversedSignalPreprocess3[reversedSignalReadIndex]
                                    case 3:
                                        pointer.assumingMemoryBound(to: Int16.self).pointee = reversedSignalPreprocess4[reversedSignalReadIndex]
                                    default:
                                        pointer.assumingMemoryBound(to: Int16.self).pointee = 0
                                }
                                
                                pointer += 2
                                
                                reversedSignalReadIndex += 1
                                if reversedSignalReadIndex >= 2048 {
                                    reversedSignalReadIndex %= 2048
                                    reversedSignalToReadFrom += 1
                                    reversedSignalToReadFrom %= 4
                                    /* we update the +2 buffer because we have pointers to current buffer **/
                                    semaphore[(reversedSignalToReadFrom+2) % 4].signal()
                                }
                            }
                        }
                        
                    } else {
                        let currentVolume = self.volume
                        var currentPhase = self.phase
                        
                        /* we increase the phase each frame */
                        let phaseIncrement = 2.0 * Float.pi * self.freq / sampleRateFloat
                    
                        var buffer = [Float](repeating: 0, count: count)
                        for i in 0..<count {
                            var sinValue = sinf(currentPhase)
                            
                            /* generate a "square" wave using harmonics */
                            if (waveType == Wave.square) {
                                sinValue += sinf(3*currentPhase)/3
                                sinValue += sinf(5*currentPhase)/5
                                sinValue += sinf(7*currentPhase)/7
                                sinValue += sinf(9*currentPhase)/9
                                sinValue += sinf(11*currentPhase)/11
                                sinValue += sinf(13*currentPhase)/13
                                sinValue += sinf(15*currentPhase)/15
                                sinValue += sinf(17*currentPhase)/17
                                sinValue += sinf(19*currentPhase)/19
                            }
                            
                            /* compute the new phase using phaseIncrement */
                            currentPhase += phaseIncrement
                                                    
                            /* to prevent overflow */
                            if (currentPhase > 2.0 * Float.pi) {
                                currentPhase -= 2.0 * Float.pi
                            }
                            
                            buffer[i] = Float(sinValue)
                            
                            /* if biquad is being used, compute low pass filter */
                            if readyForBiquad {
                                buffer[i] = lowPass.processSample(input: buffer[i])
                            }
                            
                            /* multiply by volume */
                            buffer[i] = buffer[i] * Float(currentVolume)
                                                    
                            /* convert float to pcm int16 format and store in buffer */
                            /* audio buffer is in 32-bit, we will move the pointer by two int16 */
                            if (i < (byteSizeCount / 2)) {
                                pointer.assumingMemoryBound(to: Int16.self).pointee = Int16(buffer[i])
                                pointer += 2
                            }
                        }
                        
                        /*
                         * This was originally for graphing, but it is slow
                         * and hogs memory.
                         *
                        for i in 0..<count {
                            if readyForFFTGraph {
                                for j in 0..<((fftSize/count)-1) {
                                    signal[i + count*j] = signal[i + count*(j+1)]
                                }
                                signal[i+(fftSize-count)] = buffer[i]
                            }
                        }
                        */
                        
                        /* we save the phase position each process */
                        self.phase = currentPhase
                    }
                }
            } else {
                memset(
                    audioBuffer.mData,
                    0,
                    Int(audioBuffer.mDataByteSize))
            }
        }
    }
}
