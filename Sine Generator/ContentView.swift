//
//  ContentView.swift
//  Sine Generator
//
//  GUI for the program
//
//  Created by Varshan S on 2021-02-01.
//

import SwiftUI
import Combine
import Charts


let audioUnit = SineGenerator()
var timer: Timer?


struct LineChartSwiftUI: NSViewRepresentable {
	@Binding var status: Bool
	typealias NSViewType = LineChartView
	let lineChart = LineChartView(frame: NSRect())
	
	func makeNSView(context: Context) -> LineChartView {
		setUpChart()
		return lineChart
	}
	
	func setTimer() {
		guard audioUnit.performedFFT() else {
			return
		}
		
		timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { timer in
			if audioUnit.performedFFT() && status {
				audioUnit.performFFT()
				updateChart()
			}
		}
	}
	
	func updateNSView(_ nsView: LineChartView, context: Context) {
		if status {
			setTimer()
		}
	}

	func setUpChart() {
		lineChart.noDataText = ""
		lineChart.leftAxis.drawLabelsEnabled = false
		lineChart.rightAxis.drawLabelsEnabled = false
		updateChart()
	}
	
	func updateChart() {
		let dataSets = [getLineChartDataSet()]
		let data = LineChartData(dataSets: dataSets)
		lineChart.data = data
	}

	func getLineChartDataSet() -> LineChartDataSet {
		guard audioUnit.performedFFT() else {
			return LineChartDataSet(entries: [], label: "FFT")
		}
		
		var entries: [ChartDataEntry] = []
		
		for index in 20...22000 {
			entries.append(ChartDataEntry(x: Double(index), y: Double(audioUnit.getFreqData(freq: Float(index)))))
		}
		let data = LineChartDataSet(entries: entries, label: "FFT")
		
		data.drawCirclesEnabled = false
		data.drawValuesEnabled = false
		return data
	}
	
}

struct ContentView: View {
    @State var freq: Float = 440.0
    @State var amplitude: Float = 5
    @State var cutoff: Float = 1000.0
	@State var cutoffButterworth: Float = 1000.0
	@State var resonance: Float = 1.0
	@State var dcGain: Float = 1.0
	@State var startChart: Bool = false
	@State var selectedWave = Wave.sine
	@State var enableBiquad: Bool = false
	@State var enableHann: Bool = false
	@State var enableFilter: Bool = false
	
    func frequencyChanged() {
        print("Frequency changed to \(freq)")
        audioUnit.setFrequency(newFreq: freq)
    }
    
    func volumeChanged() {
        print("Amplitude changed to \(amplitude)")
        audioUnit.setToneVolume(vol: amplitude/10)
    }
    
    func cutoffChanged() {
        print("Cutoff changed to \(cutoff)")
        audioUnit.setCutoff(cutoff: cutoff)
    }
    
    func resonanceChanged() {
        print("Resonance changed to \(resonance)")
        audioUnit.setResonance(resonance: resonance)
    }
	
	func cutoffButterworthChanged() {
		print("Cutoff changed to \(cutoffButterworth)")
		audioUnit.setButterworthCutoff(cutoff: cutoffButterworth)
	}
	
	func dcGainChanged() {
		print("DC Gain changed to \(dcGain)")
		audioUnit.setButterworthDCGain(gain: dcGain)
	}
	
	func toggleBiquad() {
		audioUnit.toggleBiquad(ready: self.enableBiquad)
	}
    
	func toggleHann() {
		audioUnit.toggleHann(ready: self.enableHann)
	}
	
    var body: some View {
        HStack {
            VStack{
				Group {
					Slider(
						value: Binding(get: {
							self.amplitude
						}, set: { (newVolume) in
							self.amplitude = newVolume
							self.volumeChanged()
						}),
						in: 0...10
					) {
						Text("Volume")
					}
					
					Slider(
						value: Binding(get: {
							self.freq
						}, set: { (newFrequency) in
							self.freq = newFrequency
							self.frequencyChanged()
						}),
						in: 20...22000,
						minimumValueLabel: Text("20"),
						maximumValueLabel: Text("22000")
					) {
						Text("Frequency")
					}
					
					Text("Current frequency: \(freq)")
					
					HStack{
						Picker("Wave", selection: $selectedWave) {
							Text("Sine").tag(Wave.sine)
							Text("Square").tag(Wave.square)
						}.onChange(of: selectedWave) {item in
							audioUnit.setWave(type: item)
						}
						
						Button(action: {
							DispatchQueue.main.async {
								audioUnit.setFrequency(newFreq: freq)
								audioUnit.setToneVolume(vol: amplitude/10)
								audioUnit.generate()
							}
						}) {
							Text("Play/Pause")
						}
					}
				}
                
                Divider().padding()
                
				Group {
					Toggle(isOn: Binding(get: {
							self.enableBiquad
						}, set: { (newBool) in
							self.enableBiquad = newBool
							self.toggleBiquad()
						})) {
						Text("Biquad low pass filter")
					}
					
					Slider(
						value: Binding(get: {
							self.cutoff
						}, set: { (newCutoff) in
							self.cutoff = newCutoff
							self.cutoffChanged()
						}),
						in: 20...22000
					) {
						Text("Cutoff frequency")
					}
					
					Slider(
						value: Binding(get: {
							self.resonance
						}, set: { (newResonance) in
							self.resonance = newResonance
							self.resonanceChanged()
						}),
						in: 0.01...5,
						minimumValueLabel: Text("0.01"),
						maximumValueLabel: Text("5")
					) {
						Text("Resonance")
					}
					
					Text("Cutoff frequency \(cutoff) with resonance \(resonance)")
				}
				
				Divider().padding()
				
				Group {
					
					Toggle(isOn: Binding(get: {
							self.enableHann
						}, set: { (newBool) in
							self.enableHann = newBool
							self.toggleHann()
						})) {
						Text("Start Hann window and Butterworth filter")
					}
					
					Slider(
						value: Binding(get: {
							self.cutoffButterworth
						}, set: { (newCutoff) in
							self.cutoffButterworth = newCutoff
							self.cutoffButterworthChanged()
						}),
						in: 20...22000
					) {
						Text("Cutoff frequency")
					}
					
					Slider(
						value: Binding(get: {
							self.dcGain
						}, set: { (newGain) in
							self.dcGain = newGain
							self.dcGainChanged()
						}),
						in: 0.01...5,
						minimumValueLabel: Text("0.01"),
						maximumValueLabel: Text("5")
					) {
						Text("DC Gain")
					}
					
					Text("Cutoff frequency \(cutoffButterworth) with DC gain \(dcGain)")
				}
				
            }.padding()
			
			  /*
            Divider()
			
			VStack {
				Button(action: {
					self.startChart.toggle()
				}) {
					Text("Start FFT Graph (slow)")
				}
								
				LineChartSwiftUI(status: $startChart)
			}.padding()
			  */
		}
    }
}



struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
        }
    }
}
