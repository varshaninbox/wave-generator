//
//  Sine_GeneratorApp.swift
//  Sine Generator
//
//  Created by Varshan S on 2021-02-01.
//

import SwiftUI

@main
struct Sine_Generator_for_CAS_748App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
