//
//  Butterworth.swift
//  Sine Generator
//
//  Code originally written by Dr. M. v. Mohrenschildt
//  Created by Varshan S on 2021-03-12.
//

import Foundation

struct Butterworth {
    static func filtGain(w: Float, w0: Float, DCGain: Float) -> Float {
        let order: Float = 6
        let gain: Float = DCGain/(sqrtf((1.0 + powf(w/w0, 2.0 * order))))
        return gain
    }
}
