//
//  FFT.swift
//  Sine Generator
//
//  Some code borrowed from
//  https://developer.apple.com/documentation/accelerate/performing_fourier_transforms_on_multiple_signals
//  Created by Varshan S on 2021-02-10.
//

import Foundation
import Accelerate

struct FFT {
    var complexSignal: DSPSplitComplex?
    var fftSetup: FFTSetup?
    
    var sampleSize: Int = 0
    var log2Size: Int = 0
    
    var real: [Float] = []
    var imaginary: [Float] = []
    
    mutating func start(size: Int) {
        sampleSize = size
        real = [Float](repeating: 0.0, count: sampleSize/2)
        imaginary = [Float](repeating: 0.0, count: sampleSize/2)
        log2Size = Int(log2f(Float(sampleSize)))
        fftSetup = vDSP_create_fftsetup(UInt(log2Size), FFTRadix(FFT_RADIX2))!
    }
    
    func end() {
        vDSP_destroy_fftsetup(fftSetup!)
    }
    
    mutating func forward(signal: [Float]) {
        real.withUnsafeMutableBufferPointer { realPtr in
            imaginary.withUnsafeMutableBufferPointer { imagPtr in
                complexSignal = DSPSplitComplex(realp: realPtr.baseAddress!,
                                                imagp: imagPtr.baseAddress!)
                
                signal.withUnsafeBytes {
                    vDSP.convert(interleavedComplexVector: [DSPComplex]($0.bindMemory(to: DSPComplex.self)),
                                 toSplitComplexVector: &complexSignal!)
                }
            }
        }
                
        vDSP_fft_zrip(fftSetup!, &(complexSignal!), 1, UInt(log2Size), Int32(FFT_FORWARD))
        
        /* scale the fft bins otherwise we will overflow */
        var scale = 1.0/Float(sampleSize*2);
        vDSP_vsmul(complexSignal!.realp, 1, &scale, complexSignal!.realp, 1, UInt(sampleSize)/2);
        vDSP_vsmul(complexSignal!.imagp, 1, &scale, complexSignal!.imagp, 1, UInt(sampleSize)/2);
    }
    
    /* for graphing only **/
    mutating func forwardAndMagnitudes(signal: [Float], magnitudes: inout [Float]) {
        forward(signal: signal)
        vDSP_zvmags(&(complexSignal!), 1, &magnitudes, 1, UInt(sampleSize/2))
    }
    
    /* for multiplying butterwoth coefficients */
    mutating func multiplyCoeff(coefficients: [Float]) {
        
        vDSP_vmul(coefficients, 1, complexSignal!.imagp, 1, complexSignal!.imagp, 1, vDSP_Length(sampleSize/2))
        vDSP_vmul(coefficients, 1, complexSignal!.realp, 1, complexSignal!.realp, 1, vDSP_Length(sampleSize/2))
        
    }
    
    mutating func inverse(signal: inout [Float]) {
        var resultAsComplex: UnsafeMutablePointer<DSPComplex>? = nil
                
        signal.withUnsafeMutableBytes {
            resultAsComplex = $0.baseAddress?.bindMemory(to: DSPComplex.self, capacity: sampleSize)
        }
        
        vDSP_fft_zrip(fftSetup!, &(complexSignal!), 1, UInt(log2Size), Int32(FFT_INVERSE))
                
        vDSP_ztoc(&(complexSignal!), 1, resultAsComplex!, 2, vDSP_Length(sampleSize/2));
    }
    
    /* Converts single point precision to int16 */
    mutating func convertFloatToInt16(floatData: inout [Float], intData: inout [Int16]) {
        vDSP_vfix16(&floatData, 1, &intData, 1, vDSP_Length(floatData.count))
    }
}
