//
//  Window.swift
//  Sine Generator
//
//  Code originally written by Dr. M. v. Mohrenschildt
//  Created by Varshan S on 2021-03-05.
//

import Foundation
import Accelerate

/* We use this to copy values */
class sharedBuffer {
    static let shared = sharedBuffer()
    var outBuffer1: [Float] = []
    var outBuffer2: [Float] = []
    var outBuffer3: [Float] = []
    var outBuffer4: [Float] = []
    
    func makeBuffer(size: Int) {
        self.outBuffer1 = [Float](repeating: 0, count: size)
        self.outBuffer2 = [Float](repeating: 0, count: size)
        self.outBuffer3 = [Float](repeating: 0, count: size)
        self.outBuffer4 = [Float](repeating: 0, count: size)
    }
}

struct Window {
    struct Hann {
        var overlapBuffer1: [Float] = []
        var overlapBuffer2: [Float] = []
        var windowBuffer1: [Float] = []
        var windowBuffer2: [Float] = []
        var wCoeff: [Float] = []
        
        var chunkSize: Int = 0
        var signal: [Float] = []
        var butterworthCoeffs: [Float] = []
        var magnitudes: [Float] = []
        
        var fft: FFT = FFT()
        var w0: Float = 0
        var DCGain: Float = 0
        var frameSize: Int = 512
        
        mutating func makeWindow(size: Int, frameSize: Int) {
            self.frameSize = frameSize
            self.chunkSize = size
            self.overlapBuffer1 = [Float](repeating: 0, count: chunkSize)
            self.overlapBuffer2 = [Float](repeating: 0, count: chunkSize)
            self.windowBuffer1 = [Float](repeating: 0, count: chunkSize)
            self.windowBuffer2 = [Float](repeating: 0, count: chunkSize)
            self.magnitudes = [Float](repeating: 0, count: chunkSize/2)
            self.wCoeff = [Float](repeating: 0, count: chunkSize)
            sharedBuffer.shared.makeBuffer(size: chunkSize * 3/2)
            self.butterworthCoeffs = [Float](repeating: 0, count: chunkSize/2)
            fft.start(size: chunkSize)
        }
        
        mutating func calculateHannCoeff() {
            for i in 0..<self.wCoeff.count {
                //self.wCoeff[i] = 0.5 * (1-cosf(Float.pi*Float(2*i/(chunkSize))))
                self.wCoeff[i] = powf(sinf(Float(i) * (Float.pi/Float(chunkSize))),2)
            }
        }
        
        mutating func calculateButterworthCoeff(w0: Float, DCGain: Float) {
            self.w0 = w0
            self.DCGain = DCGain
                        
            /* Determine w using size of fft */
            for i in 0..<butterworthCoeffs.count {
                butterworthCoeffs[i] = Butterworth.filtGain(w: Float(i) * (48000.0/2)/Float(butterworthCoeffs.count), w0: w0, DCGain: DCGain)
            }
        }
        
        mutating func shift(data: inout [Float]) {
            /* shift down the size of data in overlap buffer 2 */
            for j in (1..<chunkSize/data.count).reversed() {
                for i in (0..<data.count) {
                    overlapBuffer2[j*frameSize+i] = overlapBuffer2[(j-1)*frameSize+i]
                }
            }

            /* copy data from buffer 1 into buffer 2 */
            for i in (0..<data.count) {
                overlapBuffer2[i] = overlapBuffer1[((chunkSize/frameSize)/2-1)*frameSize+i]
            }

            /* shift  1 to make room for data */
            for j in (1..<chunkSize/data.count).reversed() {
                for i in (0..<data.count) {
                    overlapBuffer1[j*frameSize+i] = overlapBuffer1[(j-1)*frameSize+i]
                }
            }

            /* copy new data into overlap buffer 1 */
            for i in (0..<data.count) {
                overlapBuffer1[data.count-i-1] = data[i]
            }
        }
        
        mutating func processButterworth() {
            /* process our windows and apply coefficients */
            fft.forward(signal: windowBuffer1)
            fft.multiplyCoeff(coefficients: butterworthCoeffs)
            fft.inverse(signal: &windowBuffer1)
            
            fft.forward(signal: windowBuffer2)
            fft.multiplyCoeff(coefficients: butterworthCoeffs)
            fft.inverse(signal: &windowBuffer2)
        }
        
        mutating func processData(id: Int) {
            for i in 0..<overlapBuffer1.count {
                windowBuffer1[i] = overlapBuffer1[i] * self.wCoeff[i]
            }
            
            for i in 0..<overlapBuffer2.count {
                windowBuffer2[i] = overlapBuffer2[i] * self.wCoeff[i]
            }
            
            processButterworth()
            
            for i in 0..<sharedBuffer.shared.outBuffer1.count {
                if id == 0 {
                    if i < chunkSize/2 {
                        sharedBuffer.shared.outBuffer1[i] = windowBuffer1[i]
                    } else if i >= chunkSize {
                        sharedBuffer.shared.outBuffer1[i] = windowBuffer2[i-(chunkSize/2)]
                    } else {
                        sharedBuffer.shared.outBuffer1[i] = windowBuffer1[i] + windowBuffer2[i-(chunkSize/2)]
                    }
                } else if id == 1 {
                    if i < chunkSize/2 {
                        sharedBuffer.shared.outBuffer2[i] = windowBuffer1[i]
                    } else if i >= chunkSize {
                        sharedBuffer.shared.outBuffer2[i] = windowBuffer2[i-(chunkSize/2)]
                    } else {
                        sharedBuffer.shared.outBuffer2[i] = windowBuffer1[i] + windowBuffer2[i-(chunkSize/2)]
                    }
                } else if id == 2  {
                    if i < chunkSize/2 {
                        sharedBuffer.shared.outBuffer3[i] = windowBuffer1[i]
                    } else if i >= chunkSize {
                        sharedBuffer.shared.outBuffer3[i] = windowBuffer2[i-(chunkSize/2)]
                    } else {
                        sharedBuffer.shared.outBuffer3[i] = windowBuffer1[i] + windowBuffer2[i-(chunkSize/2)]
                    }
                } else {
                    if i < chunkSize/2 {
                        sharedBuffer.shared.outBuffer4[i] = windowBuffer1[i]
                    } else if i >= chunkSize {
                        sharedBuffer.shared.outBuffer4[i] = windowBuffer2[i-(chunkSize/2)]
                    } else {
                        sharedBuffer.shared.outBuffer4[i] = windowBuffer1[i] + windowBuffer2[i-(chunkSize/2)]
                    }
                }
            }
        }
    } 
}
