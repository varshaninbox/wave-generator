//
//  Biquad.swift
//  Sine Generator
//
//  Some code is borrowed from
//  https://arachnoid.com/BiQuadDesigner/index.html
//  Created by Varshan S on 2021-03-01.
//

import Foundation

struct Biquad {
    struct Coefficients {
        let b0: Float
        let b1: Float
        let b2: Float
        let a0: Float
        let a1: Float
        let a2: Float
    }
    
    struct LowPass {
        var XnZ_1: Float = 0
        var XnZ_2: Float = 0
        var YnZ_1: Float = 0
        var YnZ_2: Float = 0
        
        var coeffs: Coefficients = Coefficients(b0: 0, b1: 0, b2: 0, a0: 0, a1: 0, a2: 0)
        
        mutating func computeCoeffs(resonance: Float, cutoff: Float, sampleRate: Float) {
            let omega = 2.0 * Float.pi * cutoff / sampleRate
            let alpha = sinf(omega) / (2.0 * resonance)

            /* b0 and b2 are equivalent */
            let b0 = (1.0 - cosf(omega)) * 0.5
            let b1 = 1.0 - cosf(omega)
            let a0 = 1.0 + alpha
            let a1 = -2.0 * cosf(omega)
            let a2 = 1.0 - alpha
            
            /* normalized using a0 */
            coeffs = Coefficients(
                b0: b0/a0,
                b1: b1/a0,
                b2: b0/a0,
                a0: 1,
                a1: a1/a0,
                a2: a2/a0
            )
        }

        mutating func processSample(input: Float) -> Float {
            let Xn = input
            let Yn = coeffs.b0*Xn + coeffs.b1*XnZ_1 + coeffs.b2*XnZ_2 - coeffs.a1*YnZ_1 - coeffs.a2*YnZ_2

            XnZ_2 = XnZ_1
            XnZ_1 = Xn
            YnZ_2 = YnZ_1
            YnZ_1 = Yn
            return Yn
        }
    }
}
